﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //tämä tulostaa tekstin Hello Git!
            Console.WriteLine("Hello Git!");
            //Console.WriteLine(Environment.UserName);
            Laskin laskin1 = new Laskin();


            int summa = laskin1.Summa(5, 6);
            Console.WriteLine("Summa: " + summa);

            int summa2 = laskin1.Summa(3, 12);
            Console.WriteLine("Summa: " + summa2);
        }
    }
}
